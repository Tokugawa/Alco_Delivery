﻿using AlcoDelivery.DataModel.Models;

namespace AlcoDelivery.Operations.Abstraction
{
    public interface IExpirationTokenOperations
    {
        bool CheckExpiration(int currentUserId);

        void Update(ExpirationTokenModel item);
    }
}
