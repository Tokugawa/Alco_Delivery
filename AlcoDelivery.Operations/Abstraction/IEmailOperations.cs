﻿using AlcoDelivery.DataModel.Enums;

namespace AlcoDelivery.Operations.Abstraction
{
    public interface IEmailOperations
    {
        void CompleteRegistration(string to, string currentUrl, string confirmationUrl, string userName, Language language = Language.English);
        
        void ResetPassword(string to, string confirmationUrl, Language language = Language.English);

        void ResetPasswordConfirmation(string to);     
    }
}
