﻿using AlcoDelivery.DataModel.Models;
using System;

namespace AlcoDelivery.Operations.Abstraction
{
    public interface IAuthenticationOperations
	{
        bool IsValid(string username, string password);

        UserModel RegisterUser(AuthenticationModel user, bool isRecruiter);

        UserModel RegisterExternalUser(ExternalLoginModel model, string uploadImageUrl);

        UserModel FindUser(string username, string password);

        UserModel FindUser(ExternalLoginModel user);

        string Confirm(Guid code);

        UserModel ForgotPassword(ForgotPasswordModel item);

        bool ChangePassword(ChangePasswordModel item);

        bool ResetPassword(ResetPasswordModel item, out string userEmail);

        AuthInfoModel GetAuthInformation(int userId);
        
        void UpdateToken(ExpirationTokenModel model);

        CompleteRegistrationModel ConfirmRegistration(Guid code);
    }
}
