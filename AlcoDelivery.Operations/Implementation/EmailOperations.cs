﻿using System;
using System.IO;
using AlcoDelivery.DataAccess;
using AlcoDelivery.DataModel.Enums;
using AlcoDelivery.Operations.Abstraction;
using AlcoDelivery.DataAccess.Entities;
using AlcoDelivery.Common;

namespace AlcoDelivery.Operations.Implementation
{
    public class EmailOperations : IEmailOperations
    {
        private IUnitOfWork _unitOfWork;

        private readonly IRepository<EmailEntity> _emailRepository;

        private readonly IRepository<UserEntity> _userRepository;


        public EmailOperations(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.Repository<UserEntity>();
            _emailRepository = _unitOfWork.Repository<EmailEntity>();
        }

        private static string TemplatesDir
        {
            get
            {
                return Path.Combine(AppDomain.CurrentDomain.RelativeSearchPath, AppConfiguration.EmailTemplatesFolder);
            }
        }

        public void CompleteRegistration(string to, string currentUrl, string confirmationUrl, string userName, Language language = Language.English)
        {

        }

        public void ResetPassword(string to, string confirmationUrl, Language language = Language.English)
        {

        }

        public void ResetPasswordConfirmation(string to)
        {

        }

        private static void SendEmail(string toEmailAddress, string subject, Language language, string body)
        {

        }
    }
}
