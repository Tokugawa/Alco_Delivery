﻿using System;
using System.Linq;
using AlcoDelivery.Operations.Abstraction;
using AlcoDelivery.DataAccess;
using AlcoDelivery.DataAccess.Entities;
using AlcoDelivery.DataModel.Models;

namespace AlcoDelivery.Operations.Implementation
{
    public class ExpirationTokenOperations : IExpirationTokenOperations
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRepository<ExpirationTokenEntity> _expirationTokenRepository;

        private readonly IRepository<UserEntity> _userRepository;

        public ExpirationTokenOperations(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _expirationTokenRepository = _unitOfWork.Repository<ExpirationTokenEntity>();
            _userRepository = _unitOfWork.Repository<UserEntity>();          
        }

        public bool CheckExpiration(int currentUserId)
        {
            var expirationToken = _expirationTokenRepository.GetById(currentUserId);
            return expirationToken == null;
        }

        public void Update(ExpirationTokenModel item)
        {
            var user = _userRepository.Include(x => x.ExpirationToken).FirstOrDefault(x => x.Id == item.UserId && !x.IsDeleted);

            if (user != null)
            {
                user.ExpirationToken = new ExpirationTokenEntity
                {
                    LastActivityDateTime = DateTime.UtcNow
                };
                _unitOfWork.SaveChanges();
            }
        }
    }
}
