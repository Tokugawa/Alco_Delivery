﻿using AlcoDelivery.Operations.Abstraction;
using System;
using System.Linq;
using System.Collections.Generic;
using AlcoDelivery.Operations.Helpers;
using System.Data.Entity;
using AlcoDelivery.Common.Context.Abstraction;
using AlcoDelivery.DataAccess;
using AlcoDelivery.DataAccess.Entities;
using AlcoDelivery.DataModel.Models;
using AlcoDelivery.DataModel.Enums;

namespace AlcoDelivery.Operations.Implementation
{
    public class AuthenticationOperations : IAuthenticationOperations
    {
        private readonly ICryptographyContext _cryptographyContext;

        private readonly IPasswordContext _passwordContext;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IRepository<UserEntity> _userRepository;

        private readonly IRepository<ResetPasswordEntity> _resetPasswordRepository;

        private readonly IRepository<ExternalLoginEntity> _externalReporitory;

        public AuthenticationOperations(IUnitOfWork unitOfWork, ICryptographyContext cryptographyContext, IPasswordContext passwordContext)
        {
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.Repository<UserEntity>();
            _resetPasswordRepository = _unitOfWork.Repository<ResetPasswordEntity>();
            _externalReporitory = _unitOfWork.Repository<ExternalLoginEntity>();
            _cryptographyContext = cryptographyContext;
            _passwordContext = passwordContext;
        }

        public UserModel RegisterUser(AuthenticationModel user, bool isRecruiter)
        {
            if (_userRepository.Set.FirstOrDefault(u => u.Email == user.Email) != null)
            {
                throw new InvalidOperationException("User already exists");
            }

            byte[] salt = _cryptographyContext.GenerateRandomBytes();
            byte[] encode = _passwordContext.EncodePassword(user.Password, salt);

            UserEntity entityToInsert = new UserEntity
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = Convert.ToBase64String(encode),
                Salt = Convert.ToBase64String(salt),
                ConfirmationCode = Guid.NewGuid(),
                Role = (short)Role.Virtual
            };

            _userRepository.Insert(entityToInsert);
            _unitOfWork.SaveChanges();

            return new UserModel
            {
                BaseId = entityToInsert.Id,
                FirstName = entityToInsert.FirstName,
                LastName = entityToInsert.LastName,
                Email = entityToInsert.Email,
                Salt = entityToInsert.Salt,
                Password = entityToInsert.Password,
                Role = (Role)entityToInsert.Role,
                ConfirmationCode = entityToInsert.ConfirmationCode,
                ImagePath = entityToInsert.ImagePath
            };
        }

        public bool IsValid(string email, string password)
        {
            var user = _userRepository.Set.FirstOrDefault(u => u.Email == email);
            return user == null ? false : _passwordContext.ArePasswordsEqual(password, user.Password, user.Salt);
        }

        public UserModel FindUser(string email, string password)
        {
            var user = _userRepository.Set.AsNoTracking().Where(u => u.Email == email && u.Role != (short)Role.Virtual)
                .Select(u => new UserModel
            {
                BaseId = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Email = u.Email,
                Salt = u.Salt,
                Password = u.Password,
                Role = (Role)u.Role,
                ConfirmationCode = u.ConfirmationCode,
                ImagePath = u.ImagePath
            }).FirstOrDefault();
           
            return user == null || _passwordContext.ArePasswordsEqual(password, user.Password, user.Salt) ? user : null;
        }

        public UserModel FindUser(ExternalLoginModel model)
        {
            //uncomment
            var credentials = _externalReporitory.Include(x=>x.User).FirstOrDefault(x => x.Provider == model.Provider && x.ProviderKey == model.ProviderKey);
            return credentials == null ? null : new UserModel
            {
                BaseId = credentials.User.Id,
                FirstName = credentials.User.FirstName,
                LastName = credentials.User.LastName,
                Email = credentials.User.Email,
                ImagePath = credentials.User.ImagePath,
                Role = (Role)credentials.User.Role
            };
        }

        public UserModel RegisterExternalUser(ExternalLoginModel user, string uploadImageUrl)
        {
            var existUserWithSameEmail = _userRepository.Include(x => x.ExternalLogins).FirstOrDefault(x => x.Email == user.Email);
            if (existUserWithSameEmail != null)
            {
                if (existUserWithSameEmail.ExternalLogins == null)
                {
                    existUserWithSameEmail.ExternalLogins = new List<ExternalLoginEntity>
                    {
                        new ExternalLoginEntity
                        {
                            ProviderKey = user.ProviderKey,
                            Provider = user.Provider
                        }
                    };
                }
                else
                {
                    existUserWithSameEmail.ExternalLogins.Add(new ExternalLoginEntity
                    {
                        Provider = user.Provider,
                        ProviderKey = user.ProviderKey
                    });
                    existUserWithSameEmail.Role = 1;
                }

                _unitOfWork.SaveChanges();

                //uncomment
                return new UserModel
                {
                    BaseId = existUserWithSameEmail.Id,
                    FirstName = existUserWithSameEmail.FirstName,
                    LastName = existUserWithSameEmail.LastName,
                    Email = existUserWithSameEmail.Email,
                    ImagePath = existUserWithSameEmail.ImagePath
                };
            }

            //if (user.Provider == "Facebook")
            //{
            //    var faceBookUser = FacebookManager.GetMe(user.AccessToken);
            //    user.PictureUrl = faceBookUser != null ? faceBookUser.PictureModel.InfoModel.Url : null;
            //}
            user.PictureUrl = !string.IsNullOrEmpty(user.PictureUrl) ? $"{uploadImageUrl}/{MediaHelper.SaveFile(user.PictureUrl, FileType.Image)}" : null;

            var fullName = user.Name.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            var userToInsert = new UserEntity
            {
                FirstName = fullName[0],
                LastName = fullName[1],
                Email = user.Email,
                IsDeleted = false,
                Role = (short)Role.User,
                ImagePath = user.PictureUrl,
                ExternalLogins = new List<ExternalLoginEntity>
                {
                    new ExternalLoginEntity
                    {
                        ProviderKey = user.ProviderKey,
                        Provider = user.Provider
                    }
                }
            };

            _userRepository.Insert(userToInsert);
            _unitOfWork.SaveChanges();

            return new UserModel
            {
                BaseId = userToInsert.Id,
                FirstName = userToInsert.FirstName,
                LastName = userToInsert.LastName,
                Email = userToInsert.Email,
                ConfirmationCode = Guid.NewGuid(),
                ImagePath = userToInsert.ImagePath
            };
        }

        public void UpdateToken(ExpirationTokenModel model)
        {
            //uncomment
            //var user = _userRepository.Include(x => x.ExpirationToken).FirstOrDefault(x => x.Nickname == model.UserId && !x.IsDeleted);
            var user = _userRepository.Include(x => x.ExpirationToken).FirstOrDefault(x => x.Id == model.UserId && !x.IsDeleted);

            if (user != null)
            {
                user.ExpirationToken = new ExpirationTokenEntity
                {
                    LastActivityDateTime = DateTime.UtcNow
                };
                _unitOfWork.SaveChanges();
            }
        }

        public string Confirm(Guid code)
        {
            var virtualUser = _userRepository.Set.FirstOrDefault(x => x.ConfirmationCode == code && x.Role == (short)Role.Virtual);
            if (virtualUser != null)
            {
                virtualUser.Role = (short)Role.User;
                _unitOfWork.SaveChanges();

                return virtualUser.Email;
            }

            return null;
        }

        public CompleteRegistrationModel ConfirmRegistration(Guid code)
        {
            var virtualUser = _userRepository.Set.FirstOrDefault(x => x.ConfirmationCode == code /*&& x.Role == (short)Role.Virtual*/);
            if (virtualUser != null)
            {
                //virtualUser.Role = (short)Role.User;
                //_unitOfWork.SaveChanges();
                return new CompleteRegistrationModel
                {
                    Email = virtualUser.Email,
                    FirstName = virtualUser.FirstName,
                    LastName = virtualUser.LastName
                };
            }
            return null;
        }

        public UserModel ForgotPassword(ForgotPasswordModel item)
        {
            var user = _userRepository
                .Include(x => x.ResetPassword)
                .FirstOrDefault(x => x.Email == item.Email && !x.IsDeleted && x.Role != (short)Role.Virtual && x.Password != null);

            if (user != null)
            {
                user.ResetPassword = new ResetPasswordEntity
                {
                    ConfirmationCode = Guid.NewGuid(),
                    CreateDateTime = DateTime.UtcNow
                };

                _unitOfWork.SaveChanges();

                return new UserModel
                {
                    Email = user.Email,
                    ConfirmationCode = user.ResetPassword.ConfirmationCode
                };
            }
            return null;
        }
        
        public bool ChangePassword(ChangePasswordModel item)
        {
            var user = _userRepository.Set.Where(x => x.Id == item.UserId && !x.IsDeleted).FirstOrDefault();

            if (user != null)
            {
                var checkPassword = _passwordContext.ArePasswordsEqual(item.OldPassword, user.Password, user.Salt);
                if (checkPassword)
                {
                    var salt = _cryptographyContext.GenerateRandomBytes();
                    var encode = _passwordContext.EncodePassword(item.NewPassword, salt);

                    user.Password = Convert.ToBase64String(encode);
                    user.Salt = Convert.ToBase64String(salt);

                    _unitOfWork.SaveChanges();

                    return true;
                }
            }

            return false;
        }

        public bool ResetPassword(ResetPasswordModel item, out string userEmail)
        {
            var checkDateTime = DateTime.UtcNow.AddDays(-1);
            userEmail = null;

            var user = _userRepository
                .Include(x => x.ResetPassword)
                .FirstOrDefault(x => x.ResetPassword.ConfirmationCode == item.Code && x.ResetPassword.CreateDateTime >= checkDateTime && !x.IsDeleted);

            if (user != null)
            {
                userEmail = user.Email;

                var salt = _cryptographyContext.GenerateRandomBytes();
                var encode = _passwordContext.EncodePassword(item.Password, salt);

                user.Password = Convert.ToBase64String(encode);
                user.Salt = Convert.ToBase64String(salt);

                _resetPasswordRepository.Delete(user.ResetPassword);
                _unitOfWork.SaveChanges();

                return true;
            }

            return false;
        }

        public AuthInfoModel GetAuthInformation(int userId)
        {
            var user = _userRepository.GetById(userId);

            return new AuthInfoModel
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                ImagePath = user.ImagePath
            };
        }
    }
}
