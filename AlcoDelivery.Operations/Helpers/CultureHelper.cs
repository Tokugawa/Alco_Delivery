﻿using AlcoDelivery.DataModel.Enums;
using System.Globalization;
using System.Threading;

namespace AlcoDelivery.Operations.Helpers
{
    public static class CultureHelper
    {
        public static void SetLanguage(Language language)
        {
			switch (language)
			{
				case Language.English:
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
					break;
				default:
					Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
					break;
			}
        }
    }
}
