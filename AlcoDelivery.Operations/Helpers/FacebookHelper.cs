﻿using AlcoDelivery.DataModel.Models.Facebook;
using Facebook;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AlcoDelivery.Operations.Helpers
{
    public static class FacebookManager
    {
        public static FacebookUserModel GetMe(string accessToken)
        {
            FacebookUserModel fbUser;
            var fb = new FacebookClient { AccessToken = accessToken };
            var parameters = new Dictionary<string, object>();
            parameters["fields"] = "id,name,first_name,last_name,email,picture.type(large)";
            try
            {
                dynamic content = fb.Get("me", parameters);
                fbUser = JsonConvert.DeserializeObject<FacebookUserModel>(content.ToString());
            }
            catch
            {
                fbUser = null;
            }
            return fbUser;
        }
    }
}
