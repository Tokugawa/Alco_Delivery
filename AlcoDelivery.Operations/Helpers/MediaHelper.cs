﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using AlcoDelivery.DataModel.Enums;

namespace AlcoDelivery.Operations.Helpers
{
    public class MediaHelper
    {
        static MediaHelper()
        {
            try
            {
                const string nameDirectory = "Uploads";
                ImagePath = HttpContext.Current.Server.MapPath("~/" + nameDirectory + "/Images");
                VideoPath = HttpContext.Current.Server.MapPath("~/" + nameDirectory + "/Videos");
                SoundPath = HttpContext.Current.Server.MapPath("~/" + nameDirectory + "/Sounds");

                CreateDirectory(ImagePath);
                CreateDirectory(VideoPath);
                CreateDirectory(SoundPath);
            }
            catch (Exception exception)
            {
                throw new Exception("Cannot create uploads folder", exception);
            }
        }

        private static void CreateDirectory(string path)
        {
            var exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void DeleteFile(string fileName, FileType type, string uploadImageUrl)
        {
            string folderPath;
            switch (type)
            {
                case FileType.Image:
                    folderPath = ImagePath;
                    break;
                case FileType.Video:
                    folderPath = VideoPath;
                    break;
                case FileType.Sound:
                    folderPath = SoundPath;
                    break;
                default:
                    folderPath = null;
                    break;
            }

            if (fileName == null || folderPath == null)
            {
                throw new NullReferenceException();
            }

            var fullName = fileName.Replace(uploadImageUrl, folderPath);

            File.Delete(fullName);
        }

        public static string SaveFile(byte[] arrBytes, FileType type)
        {
            string folderPath;
            var fileName = Guid.NewGuid().ToString();
            switch (type)
            {
                case FileType.Image:
                    folderPath = ImagePath;
                    fileName += ".png";
                    break;
                case FileType.Video:
                    folderPath = VideoPath;
                    fileName += ".mov";
                    break;
                case FileType.Sound:
                    folderPath = SoundPath;
                    fileName += ".mp3";
                    break;
                default:
                    folderPath = null;
                    break;
            }

            if (folderPath == null)
            {
                throw new NullReferenceException();
            }

            using (var image = Image.FromStream(new MemoryStream(arrBytes)))
            {
                var fullName = Path.Combine(folderPath, fileName);
                image.Save(fullName, ImageFormat.Png);
            }
            return fileName;
        }

        public static string SaveFile(string url, FileType type)
        {
            string folderPath;
            var fileName = Guid.NewGuid().ToString();
            switch (type)
            {
                case FileType.Image:
                    folderPath = ImagePath;
                    fileName += ".png";
                    break;
                case FileType.Video:
                    folderPath = VideoPath;
                    fileName += ".mov";
                    break;
                case FileType.Sound:
                    folderPath = SoundPath;
                    fileName += ".mp3";
                    break;
                default:
                    folderPath = null;
                    break;
            }

            if (folderPath == null || url == null)
            {
                throw new NullReferenceException();
            }

            var webClient = new WebClient();
            var arrBytes = webClient.DownloadData(url);

            using (var image = Image.FromStream(new MemoryStream(arrBytes)))
            {
                var fullName = Path.Combine(folderPath, fileName);
                image.Save(fullName, ImageFormat.Png);
            }

            return fileName;
        }

        public static string ImagePath { get; private set; }

        public static string VideoPath { get; private set; }

        public static string SoundPath { get; private set; }
    }
}
