﻿using AlcoDelivery.Common.Context.Abstraction;
using AlcoDelivery.Common.Context.Implementation;
using AlcoDelivery.DataAccess;
using AlcoDelivery.DataAccess.Contexts;
using LightInject;

namespace AlcoDelivery.CompositionRoot
{
    public class Bootstrap
    {
        public static void Configure(ServiceContainer container)
        {
            // Common
            container.Register<ICryptographyContext, CryptographyContext>(new PerRequestLifeTime());
            container.Register<IPasswordContext, PasswordContext>(new PerRequestLifeTime());

            // Data access
            container.Register<AlcoContext, AlcoContext>();
            container.Register<IUnitOfWork, UnitOfWork>();
            container.Register(typeof(IRepository<>), typeof(Repository<>), new PerRequestLifeTime());
        }
    }
}
