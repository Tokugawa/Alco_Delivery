﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class ExpirationTokenConfiguration : EntityTypeConfiguration<ExpirationTokenEntity>
    {
        public ExpirationTokenConfiguration()
        {
            ToTable("ExpirationTokens");

            HasKey(x => x.UserId);

            Property(x => x.LastActivityDateTime)
                .IsRequired();
        }
    }
}
