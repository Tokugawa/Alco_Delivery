﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class EmailConfiguration : EntityTypeConfiguration<EmailEntity>
    {
        public EmailConfiguration()
        {
            ToTable("Emails");

            HasKey(x => x.Id);

            Property(x => x.Text);
        }
    }
}
