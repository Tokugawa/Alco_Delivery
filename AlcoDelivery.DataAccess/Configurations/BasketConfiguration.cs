﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class BasketConfiguration : EntityTypeConfiguration<BasketEntity>
    {
        public BasketConfiguration()
        {
            ToTable("Baskets");

            HasKey(x => x.UserId);

            HasMany(x => x.Products)
                .WithRequired(c => c.Basket);
        }
    }
}
