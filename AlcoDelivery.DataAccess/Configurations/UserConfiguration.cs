﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<UserEntity>
    {
        public UserConfiguration()
        {
            ToTable("Users");

            Property(x => x.FirstName)
                .IsRequired();

            Property(x => x.LastName)
                .IsRequired();

            Property(x => x.Email)
                .IsRequired();

            Property(x => x.Role)
                .IsRequired();

            Property(x => x.Password)
                .IsRequired();

            Property(x => x.Salt)
                .IsRequired();

            Property(x => x.ConfirmationCode)
                .IsRequired();

            HasOptional(x => x.ExpirationToken)
                .WithRequired();

            HasOptional(x => x.ResetPassword)
                .WithRequired();

            HasOptional(x => x.Basket)
                .WithRequired();

            HasMany(x => x.Orders)
                .WithRequired();

            HasMany(x => x.ExternalLogins)
                .WithRequired(c => c.User)
                .WillCascadeOnDelete(false);

            Property(x => x.ImagePath);
        }
    }
}
