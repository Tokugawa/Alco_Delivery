﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class OrderConfiguration : EntityTypeConfiguration<OrderEntity>
    {
        public OrderConfiguration()
        {
            ToTable("Orders");

            HasKey(x => x.Id);

            HasMany(x => x.Products)
                .WithRequired(c => c.Order);
        }
    }
}
