﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class ExternalLoginConfiguration : EntityTypeConfiguration<ExternalLoginEntity>
    {
        public ExternalLoginConfiguration()
        {
            ToTable("ExternalLogins");

            HasKey(x => x.Id);

            Property(x => x.Provider);

            Property(x => x.ProviderKey);
        }
    }
}
