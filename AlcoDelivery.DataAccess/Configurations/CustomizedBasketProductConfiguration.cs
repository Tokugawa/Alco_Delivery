﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class CustomizedBasketProductConfiguration : EntityTypeConfiguration<CustomizedBasketProductEntity>
    {
        public CustomizedBasketProductConfiguration()
        {
            ToTable("CustomizedBasketProducts");

            HasKey(x => x.Id);

            Property(x => x.Amount);

            HasRequired(x => x.Product);
        }
    }
}
