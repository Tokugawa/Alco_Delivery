﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class ResetPasswordConfiguration : EntityTypeConfiguration<ResetPasswordEntity>
    {
        public ResetPasswordConfiguration()
        {
            ToTable("ResetPasswords");

            HasKey(x => x.UserId);
        }
    }
}
