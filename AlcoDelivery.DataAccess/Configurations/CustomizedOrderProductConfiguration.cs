﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class CustomizedOrderProductConfiguration : EntityTypeConfiguration<CustomizedOrderProductEntity>
    {
        public CustomizedOrderProductConfiguration()
        {
            ToTable("CustomizedOrderProducts");

            HasKey(x => x.Id);

            Property(x => x.Amount)
                .IsRequired();

            HasRequired(x => x.Product);
        }
    }
}
