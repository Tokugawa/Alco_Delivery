﻿using AlcoDelivery.DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace AlcoDelivery.DataAccess.Configurations
{
    public class ProductConfiguration : EntityTypeConfiguration<ProductEntity>
    {
        public ProductConfiguration()
        {
            ToTable("Products");

            HasKey(x => x.Id);

            Property(x => x.Name)
                .IsRequired();

            Property(x => x.Description)
                .IsRequired();

            Property(x => x.Type)
                .IsRequired();

            Property(x => x.Region)
                .IsRequired();

            Property(x => x.Country)
                .IsRequired();

            Property(x => x.Appellation)
                .IsRequired();

            Property(x => x.Varietal)
                .IsRequired();

            Property(x => x.Price)
                .IsRequired();

            Property(x => x.Capacity)
                .IsRequired();

            Property(x => x.Strength)
                .IsRequired();

            Property(x => x.ImagePath)
                .IsRequired();
        }
    }
}
