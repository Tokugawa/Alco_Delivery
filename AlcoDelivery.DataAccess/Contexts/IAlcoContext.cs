﻿using System.Data.Entity;

namespace AlcoDelivery.DataAccess.Contexts
{
    public interface IAlcoContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}
