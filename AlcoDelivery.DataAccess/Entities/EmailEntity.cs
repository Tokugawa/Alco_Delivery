﻿using AlcoDelivery.DataAccess.Entities.Abstraction;

namespace AlcoDelivery.DataAccess.Entities
{
    public class EmailEntity : BaseEntity
    {
        public string Text { get; set; }
    }
}
