﻿namespace AlcoDelivery.DataAccess.Entities.Abstraction
{
    public abstract class DeletableEntity : BaseEntity
    {
        public bool IsDeleted { get; set; }
    }
}
