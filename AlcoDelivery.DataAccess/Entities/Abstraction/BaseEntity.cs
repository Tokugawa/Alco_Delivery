﻿namespace AlcoDelivery.DataAccess.Entities.Abstraction
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
