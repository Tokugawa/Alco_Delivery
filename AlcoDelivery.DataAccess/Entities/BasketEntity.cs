﻿using System.Collections.Generic;

namespace AlcoDelivery.DataAccess.Entities
{
    public class BasketEntity
    {
        public int UserId { get; set; }
        public List<CustomizedBasketProductEntity> Products { get; set; }
    }
}
