﻿namespace AlcoDelivery.DataAccess.Entities
{
    public class CustomizedBasketProductEntity : CustomizedProductEntity
    {
        public BasketEntity Basket { get; set; }
    }
}
