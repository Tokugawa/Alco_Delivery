﻿using AlcoDelivery.DataAccess.Entities.Abstraction;
using System;
using System.Collections.Generic;

namespace AlcoDelivery.DataAccess.Entities
{
    public class UserEntity : DeletableEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public short Role { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public Guid? ConfirmationCode { get; set; }
        public ExpirationTokenEntity ExpirationToken { get; set; }
        public ResetPasswordEntity ResetPassword { get; set; }
        public BasketEntity Basket { get; set; }
        public List<OrderEntity> Orders { get; set; }
        public List<ExternalLoginEntity> ExternalLogins { get; set; }
        public string ImagePath { get; set; }
    }
}
