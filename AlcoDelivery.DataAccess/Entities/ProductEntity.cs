﻿using AlcoDelivery.DataAccess.Entities.Abstraction;

namespace AlcoDelivery.DataAccess.Entities
{
    public class ProductEntity : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public short Type { get; set; }
        public string Region { get; set; }
        public short Country { get; set; }
        public string Appellation { get; set; }
        public short Varietal { get; set; }
        public double Price { get; set; }
        public double Capacity { get; set; }
        public double Strength { get; set; }
        public string ImagePath { get; set; }
    }
}
