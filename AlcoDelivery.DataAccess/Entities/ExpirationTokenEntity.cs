﻿using System;

namespace AlcoDelivery.DataAccess.Entities
{
    public class ExpirationTokenEntity
    {
        public int UserId { get; set; }
        public DateTime LastActivityDateTime { get; set; }
    }
}
