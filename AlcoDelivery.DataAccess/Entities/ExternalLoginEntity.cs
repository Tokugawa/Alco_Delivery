﻿using AlcoDelivery.DataAccess.Entities.Abstraction;

namespace AlcoDelivery.DataAccess.Entities
{
    public class ExternalLoginEntity : BaseEntity
    {
        public UserEntity User { get; set; }
        public string Provider { get; set; }
        public string ProviderKey { get; set; }
    }
}
