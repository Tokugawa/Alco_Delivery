﻿using AlcoDelivery.DataAccess.Entities.Abstraction;
using System.Collections.Generic;

namespace AlcoDelivery.DataAccess.Entities
{
    public class OrderEntity : BaseEntity
    {
        public UserEntity User { get; set; }
        public List<CustomizedOrderProductEntity> Products { get; set; }
    }
}
