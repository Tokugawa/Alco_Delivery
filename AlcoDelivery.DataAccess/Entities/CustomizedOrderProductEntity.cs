﻿namespace AlcoDelivery.DataAccess.Entities
{
    public class CustomizedOrderProductEntity : CustomizedProductEntity
    {
        public OrderEntity Order { get; set; }
    }
}
