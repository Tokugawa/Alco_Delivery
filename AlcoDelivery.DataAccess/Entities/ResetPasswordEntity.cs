﻿using System;

namespace AlcoDelivery.DataAccess.Entities
{
    public class ResetPasswordEntity
    {
        public int UserId { get; set; }
        public Guid ConfirmationCode { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
