﻿using AlcoDelivery.DataAccess.Entities.Abstraction;

namespace AlcoDelivery.DataAccess.Entities
{
    public class CustomizedProductEntity : BaseEntity
    {
        public ProductEntity Product { get; set; }
        public int Amount { get; set; }
    }
}
