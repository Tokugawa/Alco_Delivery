﻿namespace AlcoDelivery.DataModel.Enums
{
    public enum Role
    {
        Admin = 0,
        User,
        Virtual
    }
}
