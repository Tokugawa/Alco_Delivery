﻿namespace AlcoDelivery.DataModel.Enums
{
    public enum FileType
    {
        Image = 0,
        Video,
        Sound
    }
}
