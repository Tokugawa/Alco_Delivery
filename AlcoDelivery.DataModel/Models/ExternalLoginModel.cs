﻿namespace AlcoDelivery.DataModel.Models
{
    public class ExternalLoginModel
    {

        public ExternalLoginModel()
        {

        }

        public ExternalLoginModel(string provider, string providerKey)
        {
            Provider = provider;
            ProviderKey = providerKey;
        }

        public ExternalLoginModel(string name, string email, string provider, string providerKey, string pictureUrl)
            : this(provider, providerKey)
        {
            Name = name;
            Email = email;
            PictureUrl = pictureUrl;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Provider { get; set; }
        public string ProviderKey { get; set; }
        public string PictureUrl { get; set; }
        public string AccessToken { get; set; }
    }
}
