﻿using Newtonsoft.Json;

namespace AlcoDelivery.DataModel.Models
{
    public class AuthInfoModel
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("image_path")]
        public string ImagePath { get; set; }
    }
}
