﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace AlcoDelivery.DataModel.Models
{
    public class ResetPasswordModel
    {
        [Required]
        [JsonProperty("password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [JsonProperty("confirmation_code")]
        public Guid? Code { get; set; }
    }
}
