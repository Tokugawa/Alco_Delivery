﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AlcoDelivery.DataModel.Models
{
    public class ChangePasswordModel
    {
        public int UserId { get; set; }

        [Required]
        [JsonProperty("old_password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [JsonProperty("new_password")]
        public string NewPassword { get; set; }
    }
}
