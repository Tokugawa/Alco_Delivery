﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AlcoDelivery.DataModel.Models
{
    public class AuthenticationModel
    {
        [Required]
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [Required]
        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
