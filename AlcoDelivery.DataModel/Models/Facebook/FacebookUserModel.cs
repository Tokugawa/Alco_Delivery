﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoDelivery.DataModel.Models.Facebook
{
    public class FacebookUserModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("installed")]
        public bool Installed { get; set; }

        [JsonProperty("picture")]
        public FacebookPictureModel PictureModel { get; set; }
    }
}
