﻿using Newtonsoft.Json;

namespace AlcoDelivery.DataModel.Models.Facebook
{
    public class FacebookPictureModel
    {
        [JsonProperty("data")]
        public FacebookPictureInfoModel InfoModel { get; set; }
    }
}
