﻿using Newtonsoft.Json;

namespace AlcoDelivery.DataModel.Models.Facebook
{
    public class FacebookPictureInfoModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
