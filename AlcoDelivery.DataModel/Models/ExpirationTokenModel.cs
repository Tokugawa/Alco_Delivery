﻿using System;

namespace AlcoDelivery.DataModel.Models
{
    public class ExpirationTokenModel
    {
        public int UserId { get; set; }
        public DateTime LastActivityDateTime { get; set; }
    }
}
