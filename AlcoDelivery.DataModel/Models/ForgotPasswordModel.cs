﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AlcoDelivery.DataModel.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
