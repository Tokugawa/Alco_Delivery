﻿using AlcoDelivery.DataModel.Enums;
using System;

namespace AlcoDelivery.DataModel.Models
{
    public class UserModel
    {
        public int BaseId { get; set; }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        public Guid ImageId { get; set; }
        public Role Role { get; set; }
        public Guid? ConfirmationCode { get; set; }
        public string ImagePath { get; set; }
    }
}
