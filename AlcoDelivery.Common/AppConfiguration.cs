﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace AlcoDelivery.Common
{
    public sealed class AppConfiguration
    {
        private static string GetValue(string key)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(key))
            {
                string msg = string.Format("Cannot find {0} key in configuration file", key);

                Trace.WriteLine(msg);

                throw new InvalidProgramException(msg);
            }

            return ConfigurationManager.AppSettings[key];
        }

        public static string Log4NetConfigFile
        {
            get { return AppConfiguration.GetValue("Log4NetConfigFile"); }
        }

        public static string EmailTemplatesFolder
        {
            get { return AppConfiguration.GetValue("EmailTempatesFolder"); }
        }
    }
}
