﻿using log4net;
using log4net.Config;
using System;

namespace AlcoDelivery.Common
{
    public static class Logger
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static Logger()
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + AppConfiguration.Log4NetConfigFile;
            XmlConfigurator.Configure(new System.IO.FileInfo(filePath));
        }

        public static void WriteLine(string msg)
        {
            log.Info(msg);
        }

        public static void WriteLine(string format, params object[] args)
        {
            log.InfoFormat(format, args);
        }
    }
}
